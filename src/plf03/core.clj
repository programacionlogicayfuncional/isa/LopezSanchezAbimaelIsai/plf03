(ns plf3.core)
;comp
(defn función-comp-1
  []
  (let [f (fn [x] (max x))
        g (fn [x] (inc x))
        h (fn [x y zs] (+ y zs (dec x)))
        z (comp f g h)]
    (z 10 15 1)))
(defn función-comp-2
  []
  (let [f (fn [x] (min (* x x)))
        g (fn [x] (dec x))
        h (fn [x y] (+ y (dec x)))
        z (comp f g h)]
    (z -9 3)))
(defn función-comp-3
  []
  (let [f (fn [x] (pos? (* x x)))
        g (fn [x] (dec x))
        h (fn [x y] (+ y (dec x)))
        z (comp f g h)]
    (z -9 3)))
(defn función-comp-4
  []
  (let [f (fn [x] (pos? x))
        g (fn [x] (+ x 0))
        h (fn [x] (inc x))
        z (comp f g h)]
    (z 1)))
(defn función-comp-5
  []
  (let [f (fn [x] (+ x))
        g (fn [y] (+ y))
        h (fn [zs] (+ zs))
        z (comp f g h)]
    (z 1)))
(defn función-comp-6
  []
  (let [f (fn [x] (filter pos? x))
        g (fn [x] (filter zero? x))
        h (fn [x] (filter even? x))
        z (comp f g h)]
    (z [9])))
(defn función-comp-7
  []
  (let [f (fn [x] inc x)
        g (fn [x] inc (- x 1))
        h (fn [x y] inc (+ x y))
        z (comp f g h)]
    (z 5 1)))
(defn función-comp-8
  []
  (let [f (fn [x] str x "0")
        g (fn [x] str x "1")
        h (fn [x] str x "2")
        z (comp f g h)]
    (z "")))
(defn función-comp-9
  []
  (let [f (fn [x] (nil? x))
        g (fn [x] (int? x))
        h (fn [x] (char? x))
        z (comp f g h)]
    (z nil)))
(defn función-comp-10
  []
  (let [f (fn [x] (str x))
        g (fn [x] (char? x))
        h (fn [x] (filter char? x))
        z (comp f g h)]
    (z [nil 0 \a])))
(defn función-comp-11
  []
  (let [f (fn [x] (true? x))
        g (fn [x] (pos? x))
        h (fn [x y] (+ x y))
        z (comp f g h)]
    (z 5 8)))
(defn función-comp-12
  []
  (let [f (fn [x] (false? x))
        g (fn [x] (= [\a \b \i] x))
        h (fn [x] (take 3 x))
        z (comp f g h)]
    (z "abimael")))
(defn función-comp-13
  []
  (let [f (fn [x] (false? x))
        g (fn [x] (= [nil] x))
        h (fn [x y] (str x y))
        z (comp f g h)]
    (z "" "")))
(defn función-comp-14
  []
  (let [f (fn [x] (false? x))
        g (fn [x] (= [""] x))
        h (fn [x y] (str x y))
        z (comp f g h)]
    (z "" "")))
(defn función-comp-15
  []
  (let [f (fn [x] (true? x))
        g (fn [x] (= [\t \r \u \e] x))
        h (fn [x] (str x))
        z (comp f g h)]
    (z true)))
(if (> 1 1) 1 2)
(defn función-comp-16
  []
  (let [f (fn [x] (str x " la función"))
        g (fn [x] (if (pos? x) "es positiva" "no es positiva"))
        h (fn [x y] (+ x y))
        z (comp f g h)]
    (z 5 6)))
(defn función-comp-17
  []
  (let [f (fn [x] (str x " la función"))
        g (fn [x] (if (pos? x) "es positiva" "no es positiva"))
        h (fn [x y] (* (+ x y) -1))
        z (comp f g h)]
    (z 1 1)))
(defn función-comp-18
  []
  (let [f (fn [x] (= x \a))
        g (fn [x] (string? x))
        h (fn [x y] (get x y))
        z (comp f g h)]
    (z {:a "a" :b "b"} :a)))
(defn función-comp-19
  []
  (let [f (fn [x] (str "Dame " x " Tacos"))
        g (fn [x] (str x))
        h (fn [x y] (get x y))
        z (comp f g h)]
    (z [1 2 3 4 5 6 7 8 9 0] 5)))
(defn función-comp-20
  []
  (let [f (fn [x] (true? x))
        g (fn [x] (> x 5))
        h (fn [x] (first x))
        z (comp f g h)]
    (z #{0 1 2 3 4 5 6 7 8 9})))
(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-19)
(función-comp-20)

;complement
(defn función-complement-1
  []
  (let [f (fn [x y z] (zero? (- (- x z) y)))
        z (complement f)]
    (z 0 -1 1)))
(defn función-complement-2
  []
  (let [f (fn [x] (true? x))
        z (complement f)]
    (z 10)))
(defn función-complement-3
  []
  (let [f (fn [x] (true? x))
        z (complement f)]
    (z true)))
(defn función-complement-4
  []
  (let [f (fn [x y] (false? (>= x y)))
        z (complement f)]
    (z 12 11)))
(defn función-complement-5
  []
  (let [f (fn [x] (false? x))
        z (complement f)]
    (z false)))
(defn función-complement-6
  []
  (let [f (fn [x] (coll? x))
        z (complement f)]
    (z [10 10 01 01 10 010])))
(defn función-complement-7
  []
  (let [f (fn [x] (coll? x))
        z (complement f)]
    (z {:10 10 :01 01 :11 11})))
(defn función-complement-8
  []
  (let [f (fn [x] (coll? x))
        z (complement f)]
    (z "Abi xD")))
(defn función-complement-9
  []
  (let [f (fn [x] (true? (false? x)))
        z (complement f)]
    (z true)))
(defn función-complement-10
  []
  (let [f (fn [x y] (keyword? (get x y)))
        z (complement f)]
    (z ["a" :a \a] 1)))
(defn función-complement-11
  []
  (let [f (fn [x] (= (even? x) (pos? x)))
        z (complement f)]
    (z 15)))
(defn función-complement-12
  []
  (let [f (fn [x] (= (even? x) (neg? x)))
        z (complement f)]
    (z -14)))
(defn función-complement-13
  []
  (let [f (fn [x] (char? (str "abi" x)))
        z (complement f)]
    (z "")))
(defn función-complement-14
  []
  (let [f (fn [x] (char? (str "abi" x)))
        z (complement f)]
    (z "mael")))
(defn función-complement-15
  []
  (let [f (fn [x] (= char? (take 1 x)))
        z (complement f)]
    (z "chañe")))
(defn función-complement-16
  []
  (let [f (fn [x y] (true? (> y x)))
        z (complement f)]
    (z 3.000000001 3.000000000)))
(defn función-complement-17
  []
  (let [f (fn [x] (associative? x))
        z (complement f)]
    (z [1 2 3])))
(defn función-complement-18
  []
  (let [f (fn [x y] (boolean? (> y x)))
        z (complement f)]
    (z 8 3)))
(defn función-complement-19
  []
  (let [f (fn [x y] (float? (/ y x)))
        z (complement f)]
    (z 8.3 0.04)))
(defn función-complement-20
  []
  (let [f (fn [x] (ident? x))
        z (complement f)]
    (z {:1 1 :2 2 :3 3})))
(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)

;constantly
(defn función-constantly-1
  []
  (let [f 10
        g 10
        h (* f g)
        z (constantly h)]
    (z [1 2 3 4 5 6])))
(defn función-constantly-2
  []
  (let [f (fn [xs] (vector xs))
        g "siempre esto"
        h (fn [x] (hash-map x f))
        z (constantly g)]
    (z [1 2 3])))
(defn función-constantly-3
  []
  (let [f true
        g false
        h nil
        z (constantly h)]
    (z 007)))
(defn función-constantly-4
  []
  (let [f (fn [x] (* x x))
        g (fn [x] (/ x 10))
        h true
        z (constantly h)]
    (z [651])))
(defn función-constantly-5
  []
  (let [f "este no"
        g "este no x2"
        h "Este si"
        z (constantly h)]
    (z 5 1)))
(defn función-constantly-6
  []
  (let [f "Hacercate Bro"
        g "Mas Bro"
        h "Menos Bro xD"
        z (constantly h)]
    (z "")))
(defn función-constantly-7
  []
  (let [f (+ 10 1)
        g (* 5 3)
        h (* f g)
        z (constantly h)]
    (z nil)))
(defn función-constantly-8
  []
  (let [f (fn [x] (str x))
        g (fn [x] (char? x))
        h [1 2 3]
        z (constantly h)]
    (z [nil])))
(defn función-constantly-9
  []
  (let [f (fn [x] (nil? x))
        g (fn [x] (false? x))
        h (- (+ (- (/ 3 4) 9)) 5 1)
        z (constantly h)]
    (z nil)))
(defn función-constantly-10
  []
  (let [f {:a "a"}
        g :a
        h "a"
        z (constantly f)]
    (z nil)))
(defn función-constantly-11
  []
  (let [f {:a "a"}
        g :a
        h "a"
        z (constantly h)]
    (z nil)))
(defn función-constantly-12
  []
  (let [f {:a "a"}
        g :a
        h "a"
        z (constantly g)]
    (z nil)))
(defn función-constantly-13
  []
  (let [f (pos? 10)
        g (even? 10)
        h (= f g)
        z (constantly g)]
    (z 1111)))
(defn función-constantly-14
  []
  (let [f (neg? -10)
        g (even? 10)
        h (= f g)
        z (constantly g)]
    (z 1111)))
(defn función-constantly-15
  []
  (let [f 3.1416
        g [3.1416 f]
        h (list f g)
        z (constantly h)]
    (z 1111)))
(defn función-constantly-16
  []
  (let [f (fn [x] (str x))
        g (fn [] (char? f))
        h (g)
        z (constantly h)]
    (z 1111)))
(defn función-constantly-17
  []
  (let [f (fn [x] (get "abi" x))
        g (fn [] (char? f))
        h (g)
        z (constantly h)]
    (z 1)))
(defn función-constantly-18
  []
  (let [f (fn [x] (get "abi" x))
        g (fn [x] (str f x))
        h "nada que"
        z (constantly h)]
    (z 1)))
(defn función-constantly-19
  []
  (let [f (fn [x] (+ x x))
        g (fn [x] (- x x))
        h 0
        z (constantly h)]
    (z 0)))
(defn función-constantly-20
  []
  (let [f 2
        g (* f 1)
        h 0
        z (constantly h)]
    (z 0)))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

;every-pred
(defn función-every-pred-1
  []
  (let [f (fn [xs] (even? xs))
        g (fn [xs] (pos? xs))
        h (fn [xs] (even? (* 2 xs)))
        z (every-pred f g h)]
    (z 2)))
(defn función-every-pred-2
  []
  (let [f (fn [xs] (even? xs))
        g (fn [xs] (pos? (+ xs 1)))
        h (fn [xs] (neg? xs))
        z (every-pred f g h)]
    (z 10)))
(defn función-every-pred-3
  []
  (let [f (fn [xs] (odd? xs))
        g (fn [xs] (ratio? xs))
        h (fn [xs] (neg? xs))
        z (every-pred f g h)]
    (z 65)))
(defn función-every-pred-4
  []
  (let [f (fn [xs] (= 2 (get 0 xs)))
        g (fn [xs] (= 1 (get 1 xs)))
        h (fn [xs] (= 3 (get 2 xs)))
        z (every-pred f g h)]
    (z [2] [2 1] [2 1 3])))
(defn función-every-pred-5
  []
  (let [f (fn [xs] (zero? (- xs 1)))
        g (fn [xs] (zero? (+ xs 1)))
        h (fn [xs] (zero? (* 1 xs)))
        z (every-pred f g h)]
    (z 1)))
(defn función-every-pred-6
  []
  (let [f (fn [xs] (zero? xs))
        g (fn [xs] (zero? (+ xs 0)))
        h (fn [xs] (zero? (* xs 0)))
        z (every-pred f g h)]
    (z 0)))
(defn función-every-pred-7
  []
  (let [f (fn [xs] (char? (char xs)))
        g (fn [xs] (char? xs))
        h (fn [xs] (char? xs))
        z (every-pred f g h)]
    (z \a)))
(función-every-pred-7)
(defn función-every-pred-8
  []
  (let [f (fn [xs] (number? (- xs xs)))
        g (fn [xs] (number? (* xs xs)))
        h (fn [xs] (number? (/ xs xs xs xs)))
        z (every-pred f g h)]
    (z 007 7)))
(defn función-every-pred-9
  []
  (let [f (fn [xs] (map? xs))
        g (fn [xs] (list? xs))
        h (fn [xs] (vector? xs))
        z (every-pred f g h)]
    (z [7])))
(defn función-every-pred-10
  []
  (let [f (fn [xs] (double? (/ xs 3)))
        g (fn [xs] (double? (* xs 0.21)))
        h (fn [xs] (double? xs))
        z (every-pred f g h)]
    (z 10)))
(defn función-every-pred-11
  []
  (let [f (fn [xs] (int? (/ xs 3)))
        g (fn [xs] (int? (* xs 0.21)))
        h (fn [xs] (int? xs))
        z (every-pred f g h)]
    (z 10)))
(función-every-pred-11)
(defn función-every-pred-12
  []
  (let [f (fn [xs] (nil? (/ xs 3)))
        g (fn [xs] (nil? (* xs 0)))
        h (fn [xs] (nil? xs))
        z (every-pred f g h)]
    (z 10)))
(defn función-every-pred-13
  []
  (let [f (fn [xs] (decimal? (* xs 0.0001)))
        g (fn [xs] (decimal? (* xs 1000)))
        h (fn [xs] (decimal? xs))
        z (every-pred f g h)]
    (z 1M)))
(defn función-every-pred-14
  []
  (let [f (fn [xs] true? (> 4 xs))
        g (fn [xs] true? (> 5 xs))
        h (fn [xs] true? (> 7 xs))
        z (every-pred f g h)]
    (z 0 1 2)))
(defn función-every-pred-15
  []
  (let [f (fn [xs] true? (> 4 xs))
        g (fn [xs] true? (> 5 xs))
        h (fn [xs] true? (> 7 xs))
        z (every-pred f g h)]
    (z 0 1 2)))
(defn función-every-pred-16
  []
  (let [f (fn [xs] ident? xs)
        g (fn [ys] ident? ys)
        h (fn [zs] ident? zs)
        z (every-pred f g h)]
    (z '0)))
(defn función-every-pred-17
  []
  (let [f (fn [xs] seqable? str xs)
        g (fn [xs] char? str xs)
        h (fn [xs] number? str xs)
        z (every-pred f g h)]
    (z "abi")))
(defn función-every-pred-18
  []
  (let [f (fn [xs] set? xs)
        g (fn [xs] map? xs)
        h (fn [xs] vector? xs)
        z (every-pred f g h)]
    (z "abi")))
(defn función-every-pred-19
  []
  (let [f (fn [xs] some? (- xs xs))
        g (fn [xs] some? (+ xs 10))
        h (fn [xs] some? (/ xs 0.1))
        z (every-pred f g h)]
    (z 1)))
(defn función-every-pred-20
  []
  (let [f (fn [xs] list? xs)
        g (fn [ys] map? ys)
        h (fn [zs] vector? zs)
        z (every-pred f g h)]
    (z (list nil))))
(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

;fnil
(defn función-fnil-1
  []
  (let [f (fn [x y] (if (> x y) (+ x y) (- x y)))
        z (fnil f 50)]
    (z nil 10)))
(defn función-fnil-2
  []
  (let [f (fn [x y] (if (> x y) (+ x y) (- x y)))
        z (fnil f 50)]
    (z nil 300)))
(defn función-fnil-3
  []
  (let [f (fn [xs ys] (if (= xs ys) false true))
        z (fnil f 40)]
    (z nil 40)))
(defn función-fnil-4
  []
  (let [f (fn [xs ys] (if (nat-int? (- ys (/ xs ys))) xs ys))
        z (fnil f 45)]
    (z nil 43)))
(defn función-fnil-5
  []
  (let [f (fn [xs ys ] (if (nat-int? (+ xs -9999)) ys xs))
        z (fnil f 45)]
    (z nil false)))
(defn función-fnil-6
  []
  (let [f (fn [xs ys zs] (if (nat-int? (/ (* ys 0) 55)) xs zs))
        z (fnil f true)]
    (z nil 1 false)))
(defn función-fnil-7
  []
  (let [f (fn [xs ys zs] (if (char? xs) ys zs))
        z (fnil f \a)]
    (z nil true false)))
(defn función-fnil-8
  []
  (let [f (fn [xs ys zs] (if (char? (concat xs)) ys zs))
        z (fnil f "a")]
    (z nil true false)))
(defn función-fnil-9
  []
  (let [f (fn [xs ys zs] (if (char? (char xs)) ys zs))
        z (fnil f 10)]
    (z nil true false)))
(defn función-fnil-10
  []
  (let [f (fn [xs ys zs] (if (zero? (int xs)) ys zs))
        z (fnil f \1)]
    (z nil true false)))
(defn función-fnil-11
  []
  (let [f (fn [xs ys zs] (if (map? (vector xs)) ys zs))
        z (fnil f {:1 :2})]
    (z nil true false)))
(defn función-fnil-12
  []
  (let [f (fn [xs ys zs] (if (map? (hash-map xs xs)) ys zs))
        z (fnil f [1 2 3])]
    (z nil true false)))
(defn función-fnil-13
  []
  (let [f (fn [xs ys zs] (if (vector? (vector xs)) ys zs))
        z (fnil f (list 1 2 3 4 5 6))]
    (z nil "Es un vector" "No es un vector")))
(defn función-fnil-14
  []
  (let [f (fn [xs ys zs] (if (string? (str xs)) ys zs))
        z (fnil f "abcdef")]
    (z nil "Es String" "No es String")))
(defn función-fnil-15
  []
  (let [f (fn [xs ys zs] (if (string? (char? xs)) ys zs))
        z (fnil f "abcdef")]
    (z nil "Es String" "No es String")))
(defn función-fnil-16
  []
  (let [f (fn [xs ys zs] (if (string? xs) (zs xs) (ys xs)))
        z (fnil f "abcdef")]
    (z nil char str)))
(defn función-fnil-17
  []
  (let [f (fn [xs ys zs] (if (>= (count xs) 5) ys zs))
        z (fnil f [1 2 3 4 5 6])]
    (z nil true false)))
(defn función-fnil-18
  []
  (let [f (fn [xs ys zs] (if (list? xs) (ys xs) (zs xs xs)))
        z (fnil f [1 2 3 4 5 6])]
    (z nil conj cons)))
(defn función-fnil-19
  []
  (let [f (fn [xs ys zs] (if (>= xs 7) ys zs))
        z (fnil f 10)]
    (z nil "ya te comiste mas de 10 Tacos :V, Ya bajañe xD" "Ya no comas mas Tacos :V")))
(defn función-fnil-20
  []
  (let [f (fn [xs ys zs] (if (>= (first (sort xs)) 3) ys zs))
        z (fnil f [10 4 6 8])]
    (z nil "estas grande" "puedes jugar")))
(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)

;juxt
(defn función-juxt-1
  []
  (let [f (fn [xs] (even? xs))
        g (fn [xs] (neg? xs))
        h (fn [xs] (zero? xs))
        z (juxt f g h)]
    (z 1)))
(defn función-juxt-2
  []
  (let [f (fn [xs] (number? xs))
        g (fn [xs] (pos? xs))
        h (fn [xs] (zero? (- xs 1)))
        z (juxt f g h)]
    (z 1)))
(defn función-juxt-3
  []
  (let [f (fn [xs] (coll? xs))
        g (fn [xs] (some? xs))
        h (fn [xs] (vector? xs))
        z (juxt f g h)]
    (z [1 2 3])))
(defn función-juxt-4
  []
  (let [f (fn [xs] (true? (pos? xs)))
        g (fn [xs] (false? (pos? xs)))
        h (fn [xs] (nil? (pos? xs)))
        z (juxt f g h)]
    (z 651)))
(defn función-juxt-5
  []
  (let [f (fn [xs] (char? xs))
        g (fn [xs] (number? xs))
        h (fn [xs] (int? xs))
        z (juxt f g h)]
    (z "a")))
(defn función-juxt-6
  []
  (let [f (fn [xs] (= xs (* xs 2)))
        g (fn [xs] (= xs (+ xs xs)))
        h (fn [xs] (= xs (* xs xs)))
        z (juxt f g h)]
    (z 5)))
(defn función-juxt-7
  []
  (let [f (fn [xs ys zs] (<= xs ys zs))
        g (fn [xs ys zs] (= xs ys zs))
        h (fn [xs ys zs] (>= xs ys zs))
        z (juxt f g h)]
    (z 5 5 5)))
(defn función-juxt-8
  []
  (let [f (fn [xs] (sequential? xs))
        g (fn [xs] (some? xs))
        h (fn [xs] (seqable? xs))
        z (juxt f g h)]
    (z [1 2 3])))
(defn función-juxt-9
  []
  (let [f (fn [xs] (sequential? xs))
        g (fn [xs] (some? xs))
        h (fn [xs] (seqable? xs))
        z (juxt f g h)]
    (z (list 1 2 3))))
(defn función-juxt-10
  []
  (let [f (fn [xs] (sequential? xs))
        g (fn [xs] (some? xs))
        h (fn [xs] (seqable? xs))
        z (juxt f g h)]
    (z (hash-map 1 2 3 4))))
(defn función-juxt-11
  []
  (let [f (fn [xs] (sequential? xs))
        g (fn [xs] (some? xs))
        h (fn [xs] (seqable? xs))
        z (juxt f g h)]
    (z (hash-map [1 2 3] (list 1 2 3) #{1 2 3} 0))))
(defn función-juxt-12
  []
  (let [f (fn [xs] (set? xs))
        g (fn [xs] (seq? xs))
        h (fn [xs] (some? xs))
        z (juxt f g h)]
    (z [1 2 3 4 5 6])))
(defn función-juxt-13
  []
  (let [f (fn [xs] (vector? xs))
        g (fn [xs] (vector? (concat xs)))
        h (fn [xs] (vector? (str xs)))
        z (juxt f g h)]
    (z [1 2 3 4 5 6])))
(defn función-juxt-14
  []
  (let [f (fn [xs] (nat-int? (+ xs)))
        g (fn [xs] (nat-int? xs))
        h (fn [xs] (nat-int? (* xs -1)))
        z (juxt f g h)]
    (z 9)))
(defn función-juxt-15
  []
  (let [f (fn [xs] (pos-int? (+ xs)))
        g (fn [xs] (pos-int? xs))
        h (fn [xs] (pos-int? (* xs -1)))
        z (juxt f g h)]
    (z -1)))
(defn función-juxt-16
  []
  (let [f (fn [xs ys] (find xs ys))
        g (fn [xs ys] (find xs (+ ys 2)))
        h (fn [xs ys] (find xs (+ ys 1)))
        z (juxt f g h)]
    (z {0 \x 1 "y" 2 123 3 true} 0)))
(defn función-juxt-17
  []
  (let [f (fn [xs] (str "ven " xs))
        g (fn [xs] (str "Hacercate mas " xs))
        h (fn [xs] (str "" xs  " :c"))
        z (juxt f g h)]
    (z "Bro")))
(defn función-juxt-18
  []
  (let [f (fn [xs] (range xs))
        g (fn [xs] (range (* 5 xs)))
        h (fn [xs] (range (* 3 xs)))
        z (juxt f g h)]
    (z 2)))
(defn función-juxt-19
  []
  (let [f (fn [xs] (range xs))
        g (fn [xs] (range (* 0 xs)))
        h (fn [xs] (range (- 0 xs)))
        z (juxt f g h)]
    (z 10)))

(defn función-juxt-20
  []
  (let [f (fn [xs] (nil? xs))
        g (fn [xs] (nil? (str xs)))
        h (fn [xs] (nil? (true? xs)))
        z (juxt f g h)]
    (z nil)))
(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)

;partial
(defn función-partial-1
  []
  (let [f (fn [xs ys] (vector ys (/ xs 10)))
        z (partial f 10)]
    (z 58)))
(defn función-partial-2
  []
  (let [f (fn [xs ys] (* ys (/ xs 10)))
        z (partial f 94)]
    (z 651)))
(defn función-partial-3
  []
  (let [f (fn [xs ys] (hash-map xs ys))
        z (partial f 15)]
    (z 51)))
(defn función-partial-4
  []
  (let [f (fn [xs ys] (hash-map (/ xs ys) (* ys xs)))
        z (partial f 15)]
    (z 51)))
(defn función-partial-5
  []
  (let [f (fn [xs ys] (vector (/ xs ys) (* ys xs)))
        z (partial f 15)]
    (z 51)))
(defn función-partial-6
  []
  (let [f (fn [xs ys zs] (vector (/ xs ys) (* ys xs) (+ zs xs ys)))
        z (partial f 15 15)]
    (z 51)))
(defn función-partial-7
  []
  (let [f (fn [xs ys zs] (vector (range xs ys) (range xs ys) (range zs xs ys)))
        z (partial f 10 20)]
    (z 2)))
(defn función-partial-8
  []
  (let [f (fn [xs ys zs] (vector (range zs xs ys) (range xs) (range xs ys)))
        z (partial f 5 10)]
    (z 1)))
(defn función-partial-9
  []
  (let [f (fn [xs ys zs] (vector (nat-int? xs) (nat-int? ys) (nat-int? zs)))
        z (partial f 5 10)]
    (z 1)))
(defn función-partial-10
  []
  (let [f (fn [xs ys zs] (hash-map :1 (nat-int? xs) :2 (nat-int? ys) :3 (nat-int? zs)))
        z (partial f 5 10)]
    (z 1)))
(defn función-partial-11
  []
  (let [f (fn [xs ys zs] (str xs ys zs))
        z (partial f "a" "b")]
    (z "c")))
(defn función-partial-12
  []
  (let [f (fn [xs ys zs] (str zs (str xs ys)))
        z (partial f "a")]
    (z "b " "c")))
(defn función-partial-13
  []
  (let [f (fn [xs ys zs] (= zs (str xs ys)))
        z (partial f "a")]
    (z "b " "ab")))
(defn función-partial-14
  []
  (let [f (fn [xs ys zs] (/ zs xs ys))
        z (partial f 4)]
    (z 2 1)))
(defn función-partial-15
  []
  (let [f (fn [xs ys zs] (* zs xs ys))
        z (partial f 4)]
    (z 2 1)))
(defn función-partial-16
  []
  (let [f (fn [xs ys zs] (+ zs xs ys))
        z (partial f 4)]
    (z 2 1)))
(defn función-partial-17
  []
  (let [f (fn [xs ys zs] (- zs xs ys))
        z (partial f 4)]
    (z 2 1)))
(defn función-partial-18
  []
  (let [f (fn [xs ys zs] (* (/ xs ys) (/ zs ys)))
        z (partial f 5)]
    (z 7 7)))
(defn función-partial-19
  []
  (let [f (fn [xs ys zs] (/ (* xs ys) (* zs ys)))
        z (partial f 5)]
    (z 7 7)))
(defn función-partial-20
  []
  (let [f (fn [xs ys zs] (+ (- ys -1) (+ (* 4 xs) (* ys zs))))
        z (partial f 5)]
    (z 7 7)))
(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)

;some-fn
(defn función-some-fn-1
  []
  (let [f (fn [xs] (even? xs))
        g (fn [xs] (neg? xs))
        h (fn [xs] (zero? xs))
        z (some-fn f g h)]
    (z 1 1 1)))
(defn función-some-fn-2
  []
  (let [f (fn [xs] (even? xs))
        g (fn [xs] (even? (* xs xs)))
        h (fn [xs] (even? (* xs xs xs)))
        z (some-fn f g h)]
    (z 2)))
(defn función-some-fn-3
  []
  (let [f (fn [xs] (neg? xs))
        g (fn [xs] (neg? (* xs xs)))
        h (fn [xs] (neg? (* xs xs xs)))
        z (some-fn f g h)]
    (z -1)))
(defn función-some-fn-4
  []
  (let [f (fn [xs] (zero? xs))
        g (fn [xs ys] (zero? (* xs ys)))
        h (fn [xs ys zs] (zero? (* xs ys zs)))
        z (some-fn f g h)]
    (z 0 0 1)))
(defn función-some-fn-5
  []
  (let [f (fn [xs] (number? xs))
        g (fn [xs ys] (number? (* xs ys)))
        h (fn [xs zs] (number? (* xs zs)))
        z (some-fn f g h)]
    (z 4 6)))
(defn función-some-fn-6
  []
  (let [f (fn [xs] (odd? xs))
        g (fn [xs] (odd? xs))
        h (fn [xs] (odd? xs))
        z (some-fn f g h)]
    (z 4 6)))
(defn función-some-fn-7
  []
  (let [f (fn [xs] (some? xs))
        g (fn [xs] (some? (vector xs)))
        h (fn [xs] (some? (hash-map xs xs)))
        z (some-fn f g h)]
    (z 4)))
(defn función-some-fn-8
  []
  (let [f (fn [xs] (coll? xs))
        g (fn [xs] (coll? (vector xs)))
        h (fn [xs] (coll? (hash-map xs xs)))
        z (some-fn f g h)]
    (z 4)))
(defn función-some-fn-9
  []
  (let [f (fn [xs] (nil? xs))
        g (fn [xs] (nil? (even? xs)))
        h (fn [xs] (nil? (even? (+ xs 1))))
        z (some-fn f g h)]
    (z nil)))
(defn función-some-fn-10
  []
  (let [f (fn [xs] (neg? xs))
        g (fn [xs] (neg? (- xs 15)))
        h (fn [xs] (neg? (* -1 (+ xs 15))))
        z (some-fn f g h)]
    (z 12)))
(defn función-some-fn-11
  []
  (let [f (fn [xs] (str xs))
        g (fn [xs] (str xs " jejeje"))
        h (fn [xs ys] (str xs " + " ys))
        z (some-fn f g h)]
    (z 12 15)))
(defn función-some-fn-12
  []
  (let [f (fn [xs] (str xs xs))
        g (fn [xs] (str (char xs)))
        h (fn [xs] (str (vector xs)))
        z (some-fn f g h)]
    (z 12)))
(defn función-some-fn-13
  []
  (let [f (fn [xs] (char? xs))
        g (fn [xs] (char? (char xs)))
        h (fn [xs] (char? (vector xs)))
        z (some-fn f g h)]
    (z \a)))
(defn función-some-fn-14
  []
  (let [f (fn [xs] (char? xs))
        g (fn [xs] (char? (char xs)))
        h (fn [xs] (char? (vector xs)))
        z (some-fn f g h)]
    (z \a)))
(defn función-some-fn-15
  []
  (let [f (fn [xs] (vector xs))
        g (fn [xs] (vector xs xs))
        h (fn [xs] (vector (vector xs xs xs)))
        z (some-fn f g h)]
    (z 1)))
(defn función-some-fn-16
  []
  (let [f (fn [xs] (hash-map xs xs))
        g (fn [xs] (hash-map xs xs (hash-map xs xs) xs))
        h (fn [xs] (hash-map (vector xs xs xs) (vector xs)))
        z (some-fn f g h)]
    (z 7)))
(defn función-some-fn-17
  []
  (let [f (fn [xs] (boolean? xs))
        g (fn [xs ys] (boolean? (= ys xs)))
        h (fn [xs ys] (boolean? (if (< xs ys) xs ys)))
        z (some-fn f g h)]
    (z true false)))
(defn función-some-fn-18
  []
  (let [f (fn [xs] (boolean? xs))
        g (fn [xs ys] (boolean? (= ys xs)))
        h (fn [xs ys] (boolean? (if (= xs ys) xs ys)))
        z (some-fn f g h)]
    (z true false)))
(defn función-some-fn-19
  []
  (let [f (fn [xs] (vector xs))
        g (fn [xs ys] (vector ys xs))
        h (fn [xs ys] (vector (xs ys xs)))
        z (some-fn f g h)]
    (z true false)))
(defn función-some-fn-20
  []
  (let [f (fn [xs] (hash-map xs xs))
        g (fn [xs ys] (hash-map ys xs))
        h (fn [xs ys zs] (hash-map xs zs ys xs))
        z (some-fn f g h)]
    (z true false)))
(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)